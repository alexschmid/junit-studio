package org.launchcode.training;

import org.junit.Test;

import static org.junit.Assert.*;

public class BinarySearchTest {
    @Test
    public void testSortedNumbersInList1() {
        int[] sortedNumbers = {1,2,3,4};
        assertEquals(1, BinarySearch.binarySearch(sortedNumbers, 2));
    }
    @Test
    public void testSortedNumbersInList2() {
        int[] sortedNumbers = {1,2,3,4};
        assertEquals(3, BinarySearch.binarySearch(sortedNumbers, 4));
    }
    @Test
    public void testSortedNumbersZero() {
        int[] sortedNumbers = {1,2,3,4};
        assertEquals(-1, BinarySearch.binarySearch(sortedNumbers, 0));
    }
    @Test
    public void testSortedNumbersTooLarge() {
        int[] sortedNumbers = {1,2,3,4};
        assertEquals(-1, BinarySearch.binarySearch(sortedNumbers, 10));
    }
}
