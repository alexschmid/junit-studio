package org.launchcode.training;

import org.junit.Test;

import static org.junit.Assert.*;

public class BalancedBracketsTest {

    @Test
    public void hasBalancedBracketsLeftTextRight() {
        assertTrue(BalancedBrackets.hasBalancedBrackets("[LaunchCode]"));
    }
    @Test
    public void hasBalancedBracketsTextLeftTextRight() {
        assertTrue(BalancedBrackets.hasBalancedBrackets("Launch[Code]"));
    }
    @Test
    public void hasBalancedBracketsLeftRightText() {
        assertTrue(BalancedBrackets.hasBalancedBrackets("[]LaunchCode"));
    }
    @Test
    public void hasBalancedBracketsLeftRight() {
        assertTrue(BalancedBrackets.hasBalancedBrackets("[]"));
    }
    @Test
    public void hasBalancedBracketsLeftText() {
        assertFalse(BalancedBrackets.hasBalancedBrackets("[LaunchCode"));
    }
    @Test
    public void hasBalancedBracketsTextRightTextLeft() {
        assertFalse(BalancedBrackets.hasBalancedBrackets("Launch]Code["));
    }
    @Test
    public void hasBalancedBracketsLeft() {
        assertFalse(BalancedBrackets.hasBalancedBrackets("["));
    }
    @Test
    public void hasBalancedBracketsRightLeft() {
        assertFalse(BalancedBrackets.hasBalancedBrackets("]["));
    }

}
