package org.launchcode.training;

import org.junit.Test;

import static org.junit.Assert.*;

public class RomanNumeralTest {

    @Test
    public void romanNumeralFour() {
        assertEquals("IV", RomanNumeral.fromInt(4));
    }
    @Test
    public void romanNumeralFiftyOne() {
        assertEquals("LI", RomanNumeral.fromInt(51));
    }
    @Test
    public void romanNumeralNineHundredNinetyNine() {
        assertEquals("CMXCIX", RomanNumeral.fromInt(999));
    }
}
