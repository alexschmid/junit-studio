# JUnit Studio

This activity gives you practice with writing unit tests in Java using JUnit.

Fork this repository, and clone your fork to your machine and import the project into IntelliJ. You'll find four classes, each of which contains at least one logical error. That is, the code compiles and runs, but does not function as desired. The classes `org.launchcode.training.BalancedBrackets`, `org.launchcode.training.BinarySearch`, and `org.launchcode.training.RomanNumeral` are essentially wrappers for functions (i.e. static methods), and each of these contains one logical error. The `org.launchcode.training.Fraction` class is a proper object-oriented class, and it contains 2 logical errors.

Your task is to find all 5 errors by writing unit tests. Read the code and associated comments well enough to understand how these classes should work from an input/ouptut perspective. That is, given certain inputs, what should be the output?

Then start writing unit tests to cover all of the expected behaviors. While your goal is to find the bugs, you should approach this activitiy from the perspective of writing well-structured, thorough tests of the code.